using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereFactory : FigureAbstractFactory
{
	private Transform spawnPoint;
	public SphereFactory(Transform point)
	{
		spawnPoint = point;
	}
	public override GameObject CreateFigureStandart()
	{
		var standartSpherePrefab = Resources.Load<GameObject>("Sphere");
		var standartSphere = Instantiate(standartSpherePrefab, spawnPoint.position, Quaternion.identity);
		return standartSphere;
	}
	public override GameObject CreateFigureBig()
	{
		var bigSpherePrefab = Resources.Load<GameObject>("BigSphere");
		var bigSphere = Instantiate(bigSpherePrefab, spawnPoint.position, Quaternion.identity);
		return bigSphere;
	}
}
