using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Bin : MonoBehaviour, IObservable
{
	[SerializeField] private List<IObserver> observers;
	private void Start()
	{
		RegisterObserver(GameObject.FindGameObjectWithTag("Score").GetComponent<IObserver>());
	}                 
	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag.Equals("Figure"))
		{
			NotifyObservers();
			Destroy(collision.gameObject);
		}
	}
	public void RegisterObserver(IObserver o) => observers.Add(o);
	public void RemoveObserver(IObserver o) => observers.Remove(o);
	public void NotifyObservers()
	{
		foreach (IObserver o in observers)
		{
			o.Renew();
		}
	}


}
