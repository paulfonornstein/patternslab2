using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FigureAbstractFactory : MonoBehaviour
{
	public abstract GameObject CreateSphere();
	public abstract GameObject CreateCylinder();

}
