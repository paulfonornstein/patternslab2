using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Client : MonoBehaviour
{
	[SerializeField] private Transform point1;
	[SerializeField] private Transform point2;
	private FigureAbstractFactory factory;

	public void CreateBigSphere()
	{
		factory = new BigFactory(point1);
		factory.CreateSphere(); 
	}
	public void CreateBigCylinder()
	{
		factory = new BigFactory(point1);
		factory.CreateCylinder();
	}

	public void CreateStandartSphere()
	{
		factory = new StandartFactory(point2);
		factory.CreateSphere();
	}

	public void CreateStandartCylinder()
	{
		factory = new StandartFactory(point2);
		factory.CreateCylinder();
	}

	



}
