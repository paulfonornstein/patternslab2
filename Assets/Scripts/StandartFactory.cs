using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandartFactory : FigureAbstractFactory
{
	private Transform spawnPoint;
	public StandartFactory(Transform point)
	{
		spawnPoint = point;
	}
	public override GameObject CreateSphere()
	{
		var standartSpherePrefab = Resources.Load<GameObject>("Sphere");
		var standartSphere = Instantiate(standartSpherePrefab, spawnPoint.position, Quaternion.identity);
		return standartSphere;
	}
	public override GameObject CreateCylinder()
	{
		var standartCylinderPrefab = Resources.Load<GameObject>("Cylinder");
		var standartCylinder = Instantiate(standartCylinderPrefab, spawnPoint.position, Quaternion.identity);
		return standartCylinder;
	}
}