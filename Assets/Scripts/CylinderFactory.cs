using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CylinderFactory : FigureAbstractFactory
{
	private Transform spawnPoint;
	public CylinderFactory(Transform point)
	{
		spawnPoint = point;
	}
	public override GameObject CreateFigureStandart()
	{
		var standartCylinderPrefab = Resources.Load<GameObject>("Cylinder");
		var standartCylinder = Instantiate(standartCylinderPrefab, spawnPoint.position, Quaternion.identity);
		return standartCylinder;
	}
	public override GameObject CreateFigureBig()
	{
		var bigCylinderPrefab = Resources.Load<GameObject>("BigCylinder");
		var bigCylinder = Instantiate(bigCylinderPrefab, spawnPoint.position, Quaternion.identity);
		return bigCylinder;
	}
}
