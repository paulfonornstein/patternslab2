using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FigureMovement : MonoBehaviour
{
	[SerializeField] private float Speed;
	[SerializeField] private Transform Destination;
	private void Start()
	{
		Destination = GameObject.FindGameObjectWithTag("Bin").transform;
	}
	private void Update()
	{
		transform.position = Vector3.Lerp(transform.localPosition, Destination.position, Speed);
	}

}
