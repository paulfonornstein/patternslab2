using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour, IObserver
{
	[SerializeField] private TextMeshProUGUI scoreText;
	public void Renew() => scoreText.text = (int.Parse(scoreText.text) + 1).ToString();
}
