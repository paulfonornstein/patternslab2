using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigFactory : FigureAbstractFactory
{
	private Transform spawnPoint;
	public BigFactory(Transform point)
	{
		spawnPoint = point;
	}
	public override GameObject CreateSphere()
	{
		var bigSpherePrefab = Resources.Load<GameObject>("BigSphere");
		var bigSphere = Instantiate(bigSpherePrefab, spawnPoint.position, Quaternion.identity);
		return bigSphere;
	}
	public override GameObject CreateCylinder()
	{
		var bigCylinderPrefab = Resources.Load<GameObject>("BigCylinder");
		var bigCylinder = Instantiate(bigCylinderPrefab, spawnPoint.position, Quaternion.identity);
		return bigCylinder;
	}
}