Все скрипты находятся в Assets -> Scripts.

Интерфейсы находятся в  Assets -> Scripts -> Interfaces

IObserver - интерфейс наблюдателя.
IObservable - интерфейс наблюдаемого обьекта.

Были использованы паттерны Наблюдатель и Абстрактная фабрика.

Реализацию паттерна Наблюдатель можно посмотреть в скриптах Score и Bin.

Score реализует IObserver
Bin реализует IObservable

Реализацию паттерна Абстрактная фабрика можно посмотреть в скриптах FigureAbstractFactory, BigFactory и StandartFactory
